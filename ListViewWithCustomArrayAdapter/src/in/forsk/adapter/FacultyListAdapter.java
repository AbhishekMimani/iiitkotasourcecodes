package in.forsk.adapter;

import in.forsk.listviewwithcustomarrayadapter.R;
import in.forsk.wrapper.FacultyWrapper;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;


//ArrayAdapter - A concrete BaseAdapter that is backed by an array of arbitrary objects(wrappers mostly).
public class FacultyListAdapter extends ArrayAdapter<FacultyWrapper> {

	private final static String TAG = FacultyListAdapter.class.getSimpleName();
	
	//context to  init the layout inflater service , which inflate the custom Views(Resource layout)
	Context context;
	
	//layout resource id for the row layout(single row view)
	int resource;
	
	//to hold the data model (Instance variable )
	//as best practice ,data which is coming from an outside scope by method/constructors 
	//need to be hold for the lifetime of the class object
	//Similar we have created variable for holding context and resource id
	ArrayList<FacultyWrapper> mFacultyDataList;
	
	//Instantiates a layout XML file into its corresponding View objects.
	//In other words, it takes as input an XML file and builds the View objects from it.
	LayoutInflater inflater;

	//3rd party library to do our dirty work(background threading)
	AQuery aq;

	public FacultyListAdapter(Context context, int resource, ArrayList<FacultyWrapper> objects) {
		super(context, resource, objects);

		this.context = context;
		this.resource = resource;
		this.mFacultyDataList = (ArrayList<FacultyWrapper>) objects;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	//This is the place where you design your row view
	//this is called multiple times, depending on the data in the data model
	//this is also called , when the view needs to be recycled  
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(resource, null);
			Log.d(TAG, "New View init - "+position);
		}else {
			Log.d(TAG, "Recycling old Views - "+position);
		}
		
		
		//Retrieve the data at particular index(position)
		//so we can bind the correct data
		FacultyWrapper obj = mFacultyDataList.get(position);

		//Every time get view is called , it has to internally traverse view hierarchy (findViewById)
		//to find the correct view id and create a reference to object
		//if the row layout complexity increases, it takes hell lot of time  
		TextView nameTv = (TextView) convertView.findViewById(R.id.nameTv);
		TextView departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
		TextView reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);

		ImageView profileIv = (ImageView) convertView.findViewById(R.id.profileIv);

		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(profileIv).image(obj.getPhoto(), true, true, 200, 0);

		nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
		departmentTv.setText(obj.getDepartment());
		reserch_areaTv.setText(obj.getReserch_area());
		
		Log.d(TAG, "get View - "+position);

		return convertView;
	}

}
